package org.dvd.PSPdescargas.Descargas;

import org.dvd.PSPdescargas.Interface.Ventana;
import org.dvd.PSPdescargas.Interface.VentanaDescarga;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by k3ym4n on 26/02/2016.
 */
public class Descarga extends SwingWorker<Void,Integer>{

    private String urlD;
    private String rutaD;
    private int tamano;
    private String nombreD;
    private VentanaDescarga VD;


    public Descarga(String urlD,String rutaD,String nombreD,VentanaDescarga VD){
        this.urlD=urlD;
        this.rutaD=rutaD;
        this.nombreD=nombreD;
        this.VD = VD;
    }

    @Override
    protected Void doInBackground() throws Exception {
            URL direccion = new URL(urlD);
        URLConnection conexion = direccion.openConnection();
        tamano=conexion.getContentLength();
        String [] archivo = urlD.split("/");
        InputStream is = direccion.openStream();
        FileOutputStream fos = new FileOutputStream((rutaD + "\\" + archivo[archivo.length-1]));
        byte[] bytes = new byte[2048];


        int longi=0;
        int prog=0;
        int progB=0;
        while((longi=is.read(bytes))!= -1){
            fos.write(bytes, 0, longi);
            prog +=longi;
            //setProgress((int)(prog*100/tamano));

            if(isCancelled()){
                break;
            }
            progB+=longi;
            firePropertyChange("tamano", null, progB);
            firePropertyChange("descarga" , null,prog*100/tamano);
            Thread.sleep(100);
        }
        is.close();
        fos.close();
        setProgress(100);

        VD.setEstadodescarga("Descarga Finalizada");
        VD.anadirModelo();
        VD.guardarFichero();
        return null;
    }

    public int tamanoURL() throws IOException {
        URL url = new URL(urlD);
        HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
        int tamanoFichero = conexion.getContentLength();
        conexion.disconnect();
        return tamanoFichero;
    }
}
