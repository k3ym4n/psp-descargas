package org.dvd.PSPdescargas.Interface;


import org.dvd.PSPdescargas.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by k3ym4n on 26/02/2016.
 */
public class Ventana  implements ActionListener{

    private SplashScreen splash;
    private JPanel panel1,panel4;
    private JTextField tfurl;
    private JButton btanadir;
    private JButton btcancelar;

    private JPanel Panel4;
    private JButton btguardarEn;
    private JList listaEstado;
    private JTextField tflimiteD;

    private String ruta;
    private JFileChooser elegirRuta;

    private ArrayList<VentanaDescarga> listaDescargas;
    private DefaultListModel modeloLista;

    boolean nuevo;
    public Ventana() {
        gato();

        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        btanadir.addActionListener(this);
        btcancelar.addActionListener(this);
        btguardarEn.addActionListener(this);

        listaDescargas = new ArrayList<>();
        modeloLista = new DefaultListModel();
        listaEstado.setModel(modeloLista);

        File ficheroH = new File(Util.PATH);
        File ficheroConfi = new File(Util.PATHRUTA);
        if(ficheroH.exists()){
            cargarFichero();
        }
         if(ficheroConfi.exists()){
             leerRuta();
         }else{
             SeleccionarRuta();
         }
    }

    private void gato(){
        splash = new SplashScreen();

    }

    public void eliminarDescarga(VentanaDescarga VD){
        listaDescargas.remove(VD);
        Panel4.remove(VD.panel3);
        Panel4.repaint();
        Panel4.revalidate();
        Panel4.updateUI();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        VentanaDescarga VD = null;

        if(e.getSource()==btanadir){

            if(tflimiteD.getText().equals("")){
                JOptionPane.showMessageDialog(null, "Es obligatorio introducir un valor en el campo limite de descargas");
                return;
            }

            int numDescargas = Integer.parseInt(tflimiteD.getText());
            if (numDescargas==0){
                JOptionPane.showMessageDialog(null,"N�mero l�mite de descargas sobrepasado", "L�mite!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(numDescargas <=0 ){
                JOptionPane.showMessageDialog(null, "Es obligatorio introducir un valor en el campo limite de descargas");
                return;

            }else{
                String nombre[]=tfurl.getText().split("/");
                VD = new VentanaDescarga(tfurl.getText(),this,ruta,nombre[nombre.length-1]);
                Panel4.add(VD.panel3);
                Panel4.revalidate();
                listaDescargas.add(VD);
                VD.descargaFichero();
                tflimiteD.setText(String.valueOf(numDescargas - 1));
            }





        }else if(e.getSource()==btguardarEn){
             SeleccionarRuta();
        }else if(e.getSource()==btcancelar){



        }
    }

    private void SeleccionarRuta(){
        elegirRuta=new JFileChooser();
        elegirRuta.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if(elegirRuta.showSaveDialog(null)==JFileChooser.CANCEL_OPTION && nuevo) {
            System.exit(0);

        }
        nuevo=false;
        ruta=elegirRuta.getSelectedFile().getAbsolutePath();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter((new BufferedWriter(new FileWriter(Util.PATHRUTA, false))));
            writer.println(ruta);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void leerRuta(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Util.PATHRUTA));
            ruta=reader.readLine();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cargarFichero(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Util.PATH));
            String linea=reader.readLine();
            while (linea!=null){
                modeloLista.addElement(linea);
                linea=reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException fn){
            fn.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void guardarFichero(VentanaDescarga vd) throws IOException {
        PrintWriter writer = new PrintWriter((new BufferedWriter(new FileWriter(Util.PATH, true))));
        writer.println(vd.toString());
        writer.close();
    }

    public DefaultListModel getModel(){return modeloLista;}

    public void setModel(DefaultListModel model){ this.modeloLista=model; }

}
