package org.dvd.PSPdescargas.Interface;

import org.dvd.PSPdescargas.Descargas.Descarga;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by k3ym4n on 26/02/2016.
 */
public class VentanaDescarga implements ActionListener {

    private JProgressBar pbdescarga;
    JPanel panel3;
    private JButton btcancelar;
    private JButton btcerrar;
    private JLabel lbdescarga;
    private JLabel lbcantidad;


    private String descarga;
    private Ventana ventana;
    private String ruta;
    private String nombreDescarga;
    private Descarga Ddescarga;

    private String estadodescarga;

    //objeto File en el que metes la ruta que se ha configurado
    // en la clase Ventana en el metodo SeleccionarRuta con el objeto chooser

    /* descarga,ventana,ruta,nombreDEscarga*/
    public VentanaDescarga(String descarga, Ventana ventana, String ruta, String nombreDescarga) {
        this.descarga = descarga;
        this.ventana = ventana;
        this.ruta = ruta;
        this.nombreDescarga = nombreDescarga;

        this.estadodescarga = "";

        btcancelar.addActionListener(this);
        btcerrar.addActionListener(this);
    }

    public JProgressBar getPbdescarga() {
        return pbdescarga;
    }

    public void setPbdescarga(JProgressBar pbdescarga) {
        this.pbdescarga = pbdescarga;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == btcancelar) {
            Ddescarga.cancel(true);
            if (estadodescarga == "") {
                estadodescarga = "Cancelada";
                anadirModelo();
                guardarFichero();
            }
            } else if (e.getSource() == btcerrar) {
            ventana.eliminarDescarga(this);
            if (estadodescarga == "") {
                estadodescarga = "Cancelada";
                anadirModelo();
                guardarFichero();
            }
        }
    }

    public void descargaFichero() {
        try {

            Ddescarga = new Descarga(descarga, ruta, nombreDescarga, this);
            int total=Ddescarga.tamanoURL();
            Ddescarga.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName().equals("progress")) {
                        pbdescarga.setValue((Integer) evt.getNewValue());
                    }
                    if (evt.getPropertyName().equals("tamano")) {
                        lbcantidad.setText(String.valueOf(evt.getNewValue()) + "bytes" + "/" + total+ "bytes");
                    }
                    if (evt.getPropertyName().equals("descarga")) {
                        lbdescarga.setText(String.valueOf(evt.getNewValue()) + "%");
                    }

                }
            });
            Ddescarga.execute();
        } catch (Exception e) {
            if (e instanceof MalformedURLException) {
                JOptionPane.showMessageDialog(null, "URL incorrecta", "Descarga", JOptionPane.ERROR_MESSAGE);
            } else if (e instanceof FileNotFoundException) {
                JOptionPane.showMessageDialog(null, "No se ha encontrado el fichero", "Descarga", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error con el fichero de descarga", "Descarga", JOptionPane.ERROR_MESSAGE);
            }
            e.printStackTrace();
        }
    }

    public void anadirModelo(){
        ventana.getModel().addElement(toString());
    }

    public void guardarFichero(){
        try {
            ventana.guardarFichero(this);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return nombreDescarga+" "+ estadodescarga;
    }

    public String getEstadodescarga() {
        return estadodescarga;
    }

    public void setEstadodescarga(String estadodescarga) {
        this.estadodescarga = estadodescarga;
    }
}
