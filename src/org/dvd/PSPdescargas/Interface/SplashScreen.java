package org.dvd.PSPdescargas.Interface;



import javax.swing.*;
import java.awt.*;
import javax.swing.ImageIcon;

/**
 * Created by k3ym4n on 26/02/2016.
 */
public class SplashScreen {

    private JPanel panel;
    private JFrame splash;


    public SplashScreen(){
        splash = new JFrame("THE Gato");
        splash.getContentPane().add(panel);
        splash.setUndecorated(true);
        splash.pack();
        splash.setLocationRelativeTo(null);
        splash.setVisible(true);

        //Estas dos lineas me pillan el gif
        ImageIcon imagen = new ImageIcon("img/gato.gif");
        imagen.setImageObserver(panel);


        //tiempo que le gif estara en funcionamiento
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        splash.setVisible(false);


    }


}
